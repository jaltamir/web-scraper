Newslator
======


[![SensioLabsInsight](https://insight.sensiolabs.com/projects/b8a42bf5-e76a-41cd-b5b5-364f3d963a06/big.png)](https://insight.sensiolabs.com/projects/b8a42bf5-e76a-41cd-b5b5-364f3d963a06)


This awesome app is a news agregator. It performs scraping through some publishers.  


Installation
------------

First, clone from official repo:

`git clone git@bitbucket.org:jaltamir/prueba_runator.git`

After you have to setup the database. Information in [app/config/parameters.yml](app/config/parameters.yml)

Finally run asset install.

`php bin/console assets:install --symlink`


Documentation
-------------

The scraping can be executed by cron daemon using the ScraperCommand. You can execute it with --help option for details.

`php bin/console app:scrap:publishers --help`

Can be executed simultaneously ( using cron ) for a simultaneous scraping.

