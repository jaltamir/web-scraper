<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Feed;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class FeedRepository extends EntityRepository
{
    /**
     * @return Feed
     */
    public function getTodayFeeds()
    {
        $alias = 'feed';

        return $this->createQueryBuilder($alias)
          ->select($alias)
          ->where((new Query\Expr())->eq("$alias.date", ':now'))
          ->setParameter('now', (new \DateTime())->format('Y-m-d'))
          ->getQuery()
          ->getResult();
    }
}
