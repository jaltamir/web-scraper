
$( document ).ready(function() {
    $( document ).on( "click", ".trigger-delete-feed" ,function( event, ui )
    {
        var template = Handlebars.compile($("#handlebars_modal_feed_delete").html());
        var data = { url: $(this).attr('data-url-delete'), id: $(this).attr('data-id') };

        var $modal = $('.hook_generic').html(template(data)).children('.modal');
        $modal.modal('show');
    });
});

