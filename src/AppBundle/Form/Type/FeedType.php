<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Feed;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class FeedType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('title', null, [
            'label' => 'Title',
          ])
          ->add('body', null, [
            'label' => "New's body",
          ])
          ->add('image_file', VichFileType::class, [
            'required' => false,
            'allow_delete' => true, // not mandatory, default is true
            'download_link' => true, // not mandatory, default is true
          ])
          ->add('publisher', null, [
            'label' => 'Publisher',
          ])
          ->add('source', null, [
            'label' => 'URL Source',
          ])
          ->add('date', DateType::class, [
            'label' => 'Feed date',
            'widget' => 'single_text',
          ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
          'data_class' => Feed::class,
        ]);
    }
}
