<?php

namespace AppBundle\Common;

interface PublisherScraperInterface
{
    /**
         * @return array
         */
        public function scrapCoverNew();

        /**
         * @return string
         */
        public function getCode();
}
