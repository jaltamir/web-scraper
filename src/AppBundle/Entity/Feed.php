<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class Feed.
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FeedRepository")
 * @ORM\HasLifecycleCallbacks
 *
 * @Vich\Uploadable
 */
class Feed
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank(message="feed.title.required")
     *
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @Assert\NotBlank(message="feed.body.required")
     *
     * @var string
     */
    private $body;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="feed_image", fileNameProperty="image_name")
     *
     * @var File
     */
    private $image_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $image_name;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank(message="feed.source.required")
     *
     * @var string
     */
    private $source;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank(message="feed.publisher.required")
     *
     * @var string
     */
    private $publisher;

    /**
     * @ORM\Column(name="date",type="date",nullable=false)
     *
     * @var \DateTime
     */
    private $date;

    /**
     * @ORM\Column(name="created_datetime",type="datetime")
     *
     * @var \DateTime
     */
    private $created_datetime;

    /**
     * @ORM\Column(name="updated_datetime", type="datetime")
     *
     * @var \DateTime
     */
    private $updated_datetime;

    public function __construct()
    {
        $this->date = new \DateTime('now');
        $this->created_datetime = new \DateTime('now');
        $this->updated_datetime = new \DateTime('now');
    }

    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_datetime = new \DateTime('now');
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedDatetime()
    {
        return $this->updated_datetime;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDatetime()
    {
        return $this->created_datetime;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function setTitle($value)
    {
        $this->title = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function setBody($value)
    {
        $this->body = $value;

        return $this;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Feed
     */
    public function setImageFile(File $image = null)
    {
        $this->image_file = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updated_datetime = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->image_file;
    }

    /**
     * @param string $image_name
     *
     * @return Feed
     */
    public function setImageName($image_name)
    {
        $this->image_name = $image_name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageName()
    {
        return $this->image_name;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function setSource($value)
    {
        $this->source = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getPublisher()
    {
        return $this->publisher;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function setPublisher($value)
    {
        $this->publisher = $value;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $value
     *
     * @return $this
     */
    public function setDate(\DateTime $value)
    {
        $this->date = $value;

        return $this;
    }
}
