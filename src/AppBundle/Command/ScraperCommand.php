<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ScraperCommand.
 */
class ScraperCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
          ->setName('app:scrap:publishers')
          ->setHelp($this->getCommandHelp())
          ->setDescription('This command scraps publishers. Execute with --help option for details.')
          ->addOption('publisher', null, InputOption::VALUE_OPTIONAL, 'If set, the publisher received is scraped')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln($this->getContainer()->get('translator')->trans('Executing scraping...'));

        $publisher_option = $input->getOption('publisher');

        try {
            if ($publisher_option === null) {
                $this->getContainer()->get('scrapper_service')->scrapPublishers();
                $message = $this->getContainer()->get('translator')->trans('All publishers scraped.');
            } else {
                $this->getContainer()->get('scrapper_service')->scrapPublisher($publisher_option);
                $message = $this->getContainer()->get('translator')->trans('The publisher has been scraped.');
            }
        } catch (\Exception $ex) {
            $message = $this->getContainer()->get('translator')->trans('Error: ').$ex->getMessage();
        }
        $output->writeln($message);
    }

    private function getCommandHelp()
    {
        return <<<'HELP'
The <info>%command.name%</info> command scrap publishers and save cover new into the database:

  <info>php %command.full_name%</info>

By default all registered publishers are scraped. 

To scrap an specific publisher, add the <comment>--publisher</comment> option:

  <info>php %command.full_name%</info> <comment>--publisher="PUBLISHER_CODE"</comment>

The publishers available are: "el_pais", "la_razon", "el_periodico", "el_confidencial" and "el_mundo",

HELP;
    }
}
