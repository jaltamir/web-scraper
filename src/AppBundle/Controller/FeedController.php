<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Feed;
use AppBundle\Form\Type\FeedType;
use Doctrine\Common\Proxy\Exception\InvalidArgumentException;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FeedController extends Controller
{
    /**
     * @return Response
     *
     * @Route("/", name="feed_today")
     * @Method("GET")
     */
    public function showTodayFeedsAction()
    {
        $feeds = $this->getDoctrine()->getRepository(Feed::class)->getTodayFeeds();

        if (count($feeds) == 0) {
            $this->addFlash('warning', $this->get('translator')->trans('It seems there is no feeds registered. Maybe you need to scrap some first'));
        }

        return $this->render('AppBundle:feeds:today_feeds.html.twig', [
          'feeds' => $feeds,
        ]);
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/admin/feed/new", name="feed_new")
     * @Method({"GET","POST"})
     */
    public function newFeedAction(Request $request)
    {
        $feed = new Feed();
        $form = $this->createForm(FeedType::class, $feed, ['method' => 'POST', 'action' => $this->generateUrl('feed_new')])
          ->add('Save', SubmitType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $feed = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($feed);
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('The feed has been created'));

            return $this->redirectToRoute('feed_list');
        }

        return $this->render('@App/feeds/new_feed.html.twig', [
          'feed' => $feed,
          'form' => $form->createView(),
        ]);
    }

    /**
     * @return Response
     *
     * @Route("/admin/feed/list", name="feed_list")
     * @Method("GET")
     */
    public function getFeedsAction()
    {
        $feeds = $this->getDoctrine()->getRepository(Feed::class)->findAll();

        return $this->render('@App/feeds/list_feeds.html.twig', [
          'feeds' => $feeds,
        ]);
    }

    /**
     * @param Feed $feed
     *
     * @return Response
     *
     * @Route("/admin/feed/show/{id}", requirements={"id": "\d+"}, name="feed_show")
     * @Method("GET")
     */
    public function showFeedAction(Feed $feed)
    {
        $helper = $this->get('vich_uploader.templating.helper.uploader_helper');

        return $this->render('@App/feeds/show_feed.html.twig', [
          'feed' => $feed,
          'path' => $helper->asset($feed, 'image_file'),
        ]);
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/admin/feed/update/{id}", requirements={"id": "\d+"}, name="feed_update")
     * @Method({"GET", "POST"})
     */
    public function updateFeedAction(Feed $feed, Request $request)
    {
        $form = $this->createForm(FeedType::class, $feed, ['method' => 'POST', 'action' => $this->generateUrl('feed_update', ['id' => $feed->getId()])])
          ->add('Update', SubmitType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', $this->get('translator')->trans('The feed has been updated'));

            return $this->redirectToRoute('feed_list');
        }

        return $this->render('@App/feeds/edit_feed.html.twig', [
          'feed' => $feed,
          'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/admin/feed/delete", name="feed_delete")
     * @Method("POST")
     */
    public function deleteFeedAction(Request $request)
    {
        $feed = $this->getDoctrine()->getRepository(Feed::class)->find($request->get('id'));

        if ($feed instanceof Feed) {
            $this->getDoctrine()->getManager()->remove($feed);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', $this->get('translator')->trans('The feed has been deleted'));
        } else {
            $this->addFlash('danger', $this->get('translator')->trans('A valid feed is require for this operation'));
        }

        return $this->redirectToRoute('feed_list');
    }

    /**
     * @Route("/image/feed/{id}", requirements={"id": "\d+"}, name="load_feed_image")
     * @Method("GET")
     *
     * @param Feed $feed
     *
     * @return Response
     *
     * @throws InvalidArgumentException
     */
    public function loadFeedImageAction(Feed $feed)
    {
        return $this->get('vich_uploader.download_handler')->downloadObject($feed, 'image_file');
    }

    /**
     * @Route("/scrap-all", name="scrap_all")
     * @Method("GET")
     *
     * @return Response
     *
     * @throws InvalidArgumentException
     */
    public function scrapAllPublishersAction()
    {
        $this->get('scrapper_service')->scrapPublishers();

        $feeds = $this->getDoctrine()->getRepository(Feed::class)->findAll();

        return $this->render('@App/feeds/list_feeds.html.twig', [
          'feeds' => $feeds,
        ]);
    }
}
