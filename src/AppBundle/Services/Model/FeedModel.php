<?php

namespace AppBundle\Services\Model;

use AppBundle\Entity\Feed;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class FeedModel.
 */
class FeedModel
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var array|null
     */
    private $last_validator_errors;

    public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->translator = $translator;
        $this->validator = $validator;
        $this->last_validator_errors = [];
    }

    /**
     * @param array $params
     *
     * @return Feed|mixed
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws InvalidArgumentException
     * @throws ValidatorException
     */
    public function createOrUpdateFeed(array $params)
    {
        if (!isset($params['date']) || !$params['date'] instanceof \DateTime) {
            throw new \InvalidArgumentException($this->translator->trans('"date" parameter is missing or is not a DateTime instance'));
        }

        if (!isset($params['title'])) {
            throw new \InvalidArgumentException($this->translator->trans('"title" parameter is missing'));
        }

        if (!isset($params['body'])) {
            throw new \InvalidArgumentException($this->translator->trans('"body" parameter is missing'));
        }

        if (!isset($params['source'])) {
            throw new \InvalidArgumentException($this->translator->trans('"source" parameter is missing'));
        }

        if (!isset($params['publisher']) || !is_string($params['publisher'])) {
            throw new \InvalidArgumentException($this->translator->trans('"publisher" parameter is missing or is not valid'));
        }

        $alias = 'feed';
        $feed = $this->em->getRepository(Feed::class)
          ->createQueryBuilder($alias)
          ->where((new Expr())->andX()->addMultiple([
            (new Expr())->eq("$alias.date", ':date'),
            (new Expr())->eq("$alias.publisher", ':publisher'),
          ]))
          ->setParameter(':date', $params['date']->format('Y-m-d'))
          ->setParameter(':publisher', $params['publisher'])
          ->getQuery()
          ->getOneOrNullResult()
        ;

        if (!$feed instanceof Feed) {
            $feed = new Feed();
        }

        $feed->setDate($params['date'])
          ->setPublisher($params['publisher'])
        ->setTitle($params['title'])
        ->setSource($params['source'])
        ->setBody($params['body']);

        if (isset($params['file'])) {
            /** @var UploadedFile $file */
            $file = $params['file'];
            $feed->setImageFile($file);
        }

        $errors = $this->validator->validate($feed);

        if ($errors->count() > 0) {
            $this->last_validator_errors = [];
            foreach ($errors as $error) {
                /* @var ConstraintViolation $error */
                $this->last_validator_errors[] = $error->getMessage();
            }
            throw new ValidatorException($this->translator->trans('Feed values are not valid'));
        }

        $this->em->persist($feed);
        $this->em->flush();

        return $feed;
    }

    /**
     * @return array|null
     */
    public function getLastValidationErrors()
    {
        return $this->last_validator_errors;
    }
}
