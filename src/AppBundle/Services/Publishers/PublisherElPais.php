<?php

namespace AppBundle\Services\Publishers;

use AppBundle\Common\PublisherScraperInterface;
    use GuzzleHttp\Client;
    use Symfony\Component\DomCrawler\Crawler;
    use Symfony\Component\HttpFoundation\File\UploadedFile;

    /**
     * Class PublisherElPais.
     */
    class PublisherElPais implements PublisherScraperInterface
    {
        /**
         * Publisher URI.
         */
        const BASE_URI = 'http://elpais.com';

        /**
         * Publisher name.
         */
        const PUBLISHER_NAME = 'El País';

        /**
         * Publisher code.
         */
        const PUBLISHER_CODE = 'el_pais';

        /**
         * {@inheritdoc}
         */
        public function scrapCoverNew()
        {
            $data = [
              'date' => new \DateTime('now'),
              'publisher' => self::PUBLISHER_NAME,
            ];

            $base_uri = self::BASE_URI;

            $client = new Client(['base_uri' => $base_uri]);
            $response_main = $client->request('GET', '/index.html');
            $crawler_main = new Crawler((string) $response_main->getBody());

            $relative_url_main = $crawler_main->filter('.articulo--primero')->filter('h2')->filter('a')->attr('href');

            $data['source'] = $base_uri.$relative_url_main;

            $response = $client->request('GET', $base_uri.$relative_url_main);
            $crawler = new Crawler((string) $response->getBody(), $base_uri.$relative_url_main);

            $data['title'] = $crawler->filter('.articulo-titulo')->first()->text();
            $data['body'] = $crawler->filter('.articulo-subtitulo')->first()->text();

            try {
                $image = $crawler->filter('.articulo__contenedor')->filter('.foto')->filter('img')->first()->image();

                $file = file_get_contents($image->getUri());
                $uri_parts = explode('/', (string) $image->getUri());
                $file_name = array_pop($uri_parts);
                file_put_contents('/tmp/'.$file_name, $file);

                $data['file'] = (new UploadedFile('/tmp/'.$file_name, $file_name, null, null, null, true));
            } catch (\Exception $ex) {
                //log, send an email..
            }

            return $data;
        }

        /**
         * @return string
         */
        public function getCode()
        {
            return self::PUBLISHER_CODE;
        }
    }
