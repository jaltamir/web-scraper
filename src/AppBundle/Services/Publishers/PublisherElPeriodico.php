<?php

namespace AppBundle\Services\Publishers;

use AppBundle\Common\PublisherScraperInterface;
    use GuzzleHttp\Client;
    use Symfony\Component\DomCrawler\Crawler;
    use Symfony\Component\HttpFoundation\File\UploadedFile;

    /**
     * Class PublisherElPeriodico.
     */
    class PublisherElPeriodico implements PublisherScraperInterface
    {
        /**
         * Publisher URI.
         */
        const BASE_URI = 'http://www.elperiodico.com';

        /**
         * Publisher name.
         */
        const PUBLISHER_NAME = 'El Periódico';

        /**
         * Publisher code.
         */
        const PUBLISHER_CODE = 'el_periodico';

        /**
         * {@inheritdoc}
         */
        public function scrapCoverNew()
        {
            $data = [
                'body' => 'No resume found',
                'date' => new \DateTime('now'),
                'publisher' => self::PUBLISHER_NAME,
            ];

            $base_uri = self::BASE_URI;

            $client = new Client(['base_uri' => $base_uri]);
            $response_main = $client->request('GET', '/es/');
            $crawler_main = new Crawler((string) $response_main->getBody());

            $main_new = $crawler_main->filter('.ep-noticia')->first()->filter('h2')->filter('a');

            $data['title'] = $main_new->text();
            $data['source'] = $base_uri.$main_new->attr('href');

            try {
                $img_node = $crawler_main->filter('.ep-noticia')->first()->filter('.thumb')->filter('a');

                $crawler_main_img = new Crawler($img_node->html(), $base_uri);

                $image = $crawler_main_img->filter('img')->image();

                $file = file_get_contents($image->getUri());
                $uri_parts = explode('/', (string) $image->getUri());

                $uri_parts = explode('?', array_pop($uri_parts));

                $file_name = array_shift($uri_parts);
                file_put_contents('/tmp/'.$file_name, $file);

                $data['file'] = (new UploadedFile('/tmp/'.$file_name, $file_name, null, null, null, true));

                $data['body'] = $crawler_main->filter('.ep-noticia')->first()->filter('.subtitulo')->text();
            } catch (\Exception $ex) {
                //log, send an email..
            }

            return $data;
        }

        /**
         * @return string
         */
        public function getCode()
        {
            return self::PUBLISHER_CODE;
        }
    }
