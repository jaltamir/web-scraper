<?php

namespace AppBundle\Services\Publishers;

use AppBundle\Common\PublisherScraperInterface;
    use GuzzleHttp\Client;
    use Symfony\Component\DomCrawler\Crawler;
    use Symfony\Component\HttpFoundation\File\UploadedFile;

    /**
     * Class PublisherElMundo.
     */
    class PublisherElMundo implements PublisherScraperInterface
    {
        /**
         * Publisher URI.
         */
        const BASE_URI = 'http://www.elmundo.es';

        /**
         * Publisher name.
         */
        const PUBLISHER_NAME = 'El Mundo';

        /**
         * Publisher code.
         */
        const PUBLISHER_CODE = 'el_mundo';

        /**
         * {@inheritdoc}
         */
        public function scrapCoverNew()
        {
            $data = [
                'body' => 'No resume found',
                'date' => new \DateTime('now'),
                'publisher' => self::PUBLISHER_NAME,
            ];

            $base_uri = self::BASE_URI;

            $client = new Client(['base_uri' => $base_uri]);
            $response_main = $client->request('GET', '/index.html');
            $crawler_main = new Crawler((string) $response_main->getBody());

            $main_new = $crawler_main->filter('.mod-news')->first()->filter('.mod-header')->filter('h3')->filter('a');

            $data['title'] = $main_new->text();
            $data['source'] = $main_new->attr('href');

            try {
                $crawler_main = new Crawler((string) $response_main->getBody());

                $tmp_node = $crawler_main->filter('.mod-news')->first()->filter('.multimedia-item')->filter('a');
                $crawler_img = new Crawler($tmp_node->html(), $base_uri);

                $image = $crawler_img->filter('img')->image();

                $file = file_get_contents($image->getUri());
                $uri_parts = explode('/', (string) $image->getUri());
                $file_name = array_pop($uri_parts);
                file_put_contents('/tmp/'.$file_name, $file);

                $data['file'] = (new UploadedFile('/tmp/'.$file_name, $file_name, null, null, null, true));
            } catch (\Exception $ex) {
                //log, send an email..
            }

            return $data;
        }

        /**
         * @return string
         */
        public function getCode()
        {
            return self::PUBLISHER_CODE;
        }
    }
