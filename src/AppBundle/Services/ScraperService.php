<?php

namespace AppBundle\Services;

use AppBundle\Common\PublisherScraperInterface;
use AppBundle\Services\Model\FeedModel;
use Doctrine\Common\Proxy\Exception\InvalidArgumentException;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class ScraperService.
 */
class ScraperService
{
    /**
     * @var FeedModel
     */
    private $model_feed;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var array PublisherScraperInterface
     */
    private $publishers = [];

    public function __construct(FeedModel $model_feed, TranslatorInterface $translator, array $publishers)
    {
        $this->model_feed = $model_feed;
        $this->translator = $translator;

        foreach ($publishers as $publisher) {
            if (!$publisher instanceof PublisherScraperInterface) {
                throw new \InvalidArgumentException($this->translator->trans('The service injected is no valid'));
            }
            $this->publishers[$publisher->getCode()] = $publisher;
        }
    }

    /**
     * Scrap all registered publishers.
     */
    public function scrapPublishers()
    {
        foreach ($this->publishers as $publisher) {
            $this->model_feed->createOrUpdateFeed($publisher->scrapCoverNew());
        }
    }

    /**
     * @param $publisher_code
     *
     * @throws InvalidArgumentException
     */
    public function scrapPublisher($publisher_code)
    {
        if (!in_array($publisher_code, array_keys($this->publishers))) {
            dump($this->publishers);
            throw new \InvalidArgumentException($this->translator->trans('The publisher code received is not registered'));
        }
        $this->model_feed->createOrUpdateFeed($this->publishers[$publisher_code]->scrapCoverNew());
    }
}
